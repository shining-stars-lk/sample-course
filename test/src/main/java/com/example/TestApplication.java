package com.example;

import com.example.postprocessor.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class TestApplication {
    
    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = 
                SpringApplication.run(TestApplication.class, args);
        Test test = applicationContext.getBean("test", Test.class);
        System.out.println(test);
    }
}
