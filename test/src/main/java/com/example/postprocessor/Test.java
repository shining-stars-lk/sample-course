package com.example.postprocessor;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-03-14
 **/
@Data
@Component
public class Test {
    
    private String name;
    
    public Test() {
        System.out.println("Test构造方法");
    }
    
    @Override
    public String toString() {
        return "Test{" + "name='" + name + '\'' + '}';
    }
}
