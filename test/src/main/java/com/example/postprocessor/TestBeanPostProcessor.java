package com.example.postprocessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-03-14
 **/
@Component
public class TestBeanPostProcessor implements BeanPostProcessor {
    
    @Override
    public Object postProcessBeforeInitialization(final Object bean, final String beanName) throws BeansException {
        if (bean instanceof Test) {
            System.out.println("后置处理器执行before方法 test对象:"+bean);
        }
        return bean;
    }
    
    @Override
    public Object postProcessAfterInitialization(final Object bean, final String beanName) {
        if (bean instanceof Test) {
            Test test = (Test) bean;
            test.setName("k");
            System.out.println("后置处理器执行after方法 test对象:"+test);
            return test;
        }
        return bean;
    }
}
