package com.example.proxy.general;

import com.example.aop.Person;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2024-11-06
 **/
public class BoyProxy implements Person {
    
    private Person person;
    
    public BoyProxy(Person person) {
        this.person = person;
    }
    
    @Override
    public void goOut() {
        System.out.println("帮你挑选干净帅气的衣服");
        System.out.println("帮你挑选干净的鞋");
        person.goOut();
    }
    
    @Override
    public void goToWork() {
        System.out.println("帮你挑选干净帅气的衣服");
        System.out.println("帮你挑选干净的鞋");
        person.goToWork();
    }
}
