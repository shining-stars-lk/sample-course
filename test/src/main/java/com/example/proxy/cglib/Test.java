package com.example.proxy.cglib;

import com.example.aop.Boy;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2024-11-05
 **/
public class Test {
    
    public static void main(String[] args) {
        Boy boy = new Boy();
        CglibInterceptor cglibInterceptor = new CglibInterceptor(boy);
        Boy boyProxy = (Boy) cglibInterceptor.getProxy();
        boyProxy.goOut();
        System.out.println("");
        //boyProxy.goToWork();
    }
}
