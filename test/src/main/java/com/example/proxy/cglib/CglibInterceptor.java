package com.example.proxy.cglib;


import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;
import java.lang.reflect.Method;
/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2024-11-05
 **/
public class CglibInterceptor implements MethodInterceptor {
    
    private Object target;
    
    public CglibInterceptor(Object target) {
        this.target = target;
    }
    
    @Override     
    public Object intercept(Object object, Method method, Object[] objects,
                            MethodProxy methodProxy) throws Throwable {
        System.out.println("帮你换上干净帅气的衣服");
        System.out.println("帮你换上干净的鞋");
        return methodProxy.invoke(target, objects);
    }
    
    public Object getProxy(){
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(target.getClass());
        enhancer.setCallback(this);
        return enhancer.create();
    }
}
