package com.example.proxy.jdk;

import com.example.aop.Boy;
import com.example.aop.Person;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2024-11-05
 **/
public class test {
    
    public static void main(String[] args) {
        Boy boy = new Boy();
        JdkHandler jdkHandler = new JdkHandler(boy);
        Person person = (Person)jdkHandler.getProxy();
        person.goOut();
        System.out.println("");
        //person.goToWork();
    }
}
