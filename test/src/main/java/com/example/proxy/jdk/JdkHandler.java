package com.example.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2024-11-05
 **/
public class JdkHandler implements InvocationHandler {
    
    private Object target;
    
    public JdkHandler(Object target) {
        this.target = target;
    }
    
    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        System.out.println("帮你换上干净帅气的衣服");
        System.out.println("帮你换上干净的鞋");
        return method.invoke(target, args);
    }
    
    public Object getProxy() {
        return Proxy.newProxyInstance(this.getClass().getClassLoader(), target.getClass().getInterfaces(), 
                this);
    }
}
