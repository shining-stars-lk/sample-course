package com.example.controller;

import com.example.module.Student;
import com.example.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2024-12-17
 **/
@Slf4j
@RestController
@RequestMapping(value="student")
public class StudentController2 {
    
    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "get")
    public Student get(@RequestParam("id")Long id){
        log.info("获取学生信息");
        Student student = new Student();
        student.setName("小红");
        return student;
    }
}
