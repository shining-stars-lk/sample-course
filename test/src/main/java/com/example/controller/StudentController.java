//package com.example.controller;
//
//import com.example.module.Student;
//import com.example.service.StudentService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationContext;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.Map;
//import java.util.Map.Entry;
//import java.util.Set;
//
///**
// * @program: sample-course
// * @description:
// * @author: k
// * @create: 2024-12-17
// **/
//@RestController
//@RequestMapping(value="student")
//public class StudentController {
//
//    @Autowired
//    private Map<String,StudentService> studentServiceMap;
//
//    @Autowired
//    private ApplicationContext applicationContext;
//
//    @RequestMapping(value = "get")
//    public Student get(@RequestParam("id")Long id){
//        String serviceName =
//                applicationContext.getEnvironment().getProperty("service.name");
//        StudentService studentService = studentServiceMap.get(serviceName);
//        return studentService.get(id);
//    }
//}
