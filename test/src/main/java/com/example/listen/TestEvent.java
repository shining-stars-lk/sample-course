package com.example.listen;

import org.springframework.context.ApplicationEvent;


public class TestEvent extends ApplicationEvent {
    
    public TestEvent(final Object source) {
        super(source);
    }
}