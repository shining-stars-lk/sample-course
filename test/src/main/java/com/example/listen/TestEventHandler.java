package com.example.listen;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class TestEventHandler implements ApplicationListener<TestEvent> {
    @Override   
    public void onApplicationEvent(final TestEvent event) { 
        System.out.println("TestEventHandler监听到事件：" + event);
    }
}
