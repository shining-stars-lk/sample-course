package com.example.listen;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-03-13
 **/
@Component
public class TestEventPush implements ApplicationContextAware {
    
    private ApplicationContext applicationContext;
    
    @PostConstruct    
    public void push(){
        System.out.println("TestEventPush发布了事件");
        applicationContext.publishEvent(new TestEvent(this));
    }
    
    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
