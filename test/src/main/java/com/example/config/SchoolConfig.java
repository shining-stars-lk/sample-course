package com.example.config;

import com.example.module.School;
import com.example.module.Student;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;

//@Order(2)
//@AutoConfigureAfter(StudentConfig.class)
//@Configuration
public class SchoolConfig {
    
    
    @ConditionalOnBean(Student.class)
    @Bean
    public School school(){
        return new School();
    }
}
