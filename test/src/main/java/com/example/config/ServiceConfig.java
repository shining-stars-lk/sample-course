package com.example.config;

import com.example.service.StudentService;
import com.example.service.impl.StudentServiceXiaoHong;
import com.example.service.impl.StudentServiceXiaoMing;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


public class ServiceConfig {

    @Bean
    @ConditionalOnProperty(name="partner.name",havingValue = "xiaoMing")
    public StudentService studentServiceXiaoMing() {
        return new StudentServiceXiaoMing();
    }

    @Bean
    @ConditionalOnProperty(name="partner.name",havingValue = "xiaoHong")
    public StudentService studentServiceXiaoHong() {
        return new StudentServiceXiaoHong();
    }
}
