package com.example.idempotent;

import org.springframework.stereotype.Service;

@Service
public class PhoneService {

    public Integer getPhoneCount(Long phoneId) {
        return 1;
    }

    public boolean updatePhoneCount(Long phoneId, Integer count) {
        return true;
    }
}
