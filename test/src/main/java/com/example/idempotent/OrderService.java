package com.example.idempotent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    @Autowired
    private PhoneService phoneService;

    public boolean createOrder(CreateOrderDto dto) {
        Integer phoneCount =
                phoneService.getPhoneCount(dto.getPhoneId());
        if (phoneCount == 0) {
            return false;
        }
        int newPhoneCount = phoneCount - dto.getPurchaseCount();
        if (newPhoneCount < 0) {
            return false;
        }
        boolean result =
                phoneService.updatePhoneCount(dto.getPhoneId(), newPhoneCount);
        if (result) {
            return doCreateOrder(dto);
        }
        return false;
    }

    public boolean doCreateOrder(CreateOrderDto dto){
        return true;
    }

}
