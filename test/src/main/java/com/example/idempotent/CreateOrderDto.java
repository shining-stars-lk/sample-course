package com.example.idempotent;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class CreateOrderDto {

    private Long phoneId;

    private Long userId;

    private Integer purchaseCount;
}
