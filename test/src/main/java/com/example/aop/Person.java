package com.example.aop;

/**
 * @program: sample-course
 * @description:
 * @author: lk
 * @create: 2024-11-05
 **/
public interface Person {
    
    void goOut();
    
    void goToWork();
}
