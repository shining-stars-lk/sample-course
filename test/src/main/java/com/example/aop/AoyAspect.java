package com.example.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2024-11-07
 **/
//@Aspect
//@Component
public class AoyAspect {

    @Pointcut("execution(public * com.example.aop.*.*(..))")
    public void boyPointcut(){}
    
    @Before("boyPointcut()")
    public void before(JoinPoint joinPoint) {
        System.out.println("c前置通知执行");
    }
    
    @After("boyPointcut()")
    public void after(JoinPoint joinPoint){
        System.out.println("c后置通知执行");
    }
    
    @AfterReturning("boyPointcut()")
    public void afterReturning(JoinPoint joinPoint){
        System.out.println("c返回通知执行");
    }
    
    @AfterThrowing("boyPointcut()")
    public void afterThrowing(JoinPoint joinPoint){
        System.out.println("c异常通知执行");
    }
    
    @Around("boyPointcut()")
    public Object deAround(ProceedingJoinPoint joinPoint) throws Throwable{
        System.out.println("c环绕通知-原有方法前-执行");
        Object result = joinPoint.proceed();
        System.out.println("c环绕通知-原有方法后-执行");
        return result;
    }
}
