package com.example.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2024-11-07
 **/
//@Aspect
//@Component
public class BoyAspect {

    @Pointcut("execution(public * com.example.aop.*.*(..))")
    public void boyPointcut(){}
    
    @Before("boyPointcut()")
    public void before(JoinPoint joinPoint) {
        System.out.println("帮你挑选干净帅气的衣服");
        System.out.println("帮你挑选干净的鞋");
    }
    
    //@After("boyPointcut()")
    public void after(JoinPoint joinPoint){
        System.out.println("后置通知执行");
    }
    
    //@AfterReturning("boyPointcut()")
    public void afterReturning(JoinPoint joinPoint){
        System.out.println("返回通知执行");
    }
    
    //@AfterThrowing("boyPointcut()")
    public void afterThrowing(JoinPoint joinPoint){
        System.out.println("异常通知执行");
    }
    
    //@Around("boyPointcut()")
    public Object deAround(ProceedingJoinPoint joinPoint) throws Throwable{
        System.out.println("环绕通知-原有方法前-执行");
        Object result = joinPoint.proceed();
        System.out.println("环绕通知-原有方法后-执行");
        return result;
    }
}
