package com.example.aop;

import com.example.aop.annotion.Test;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Before;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2024-11-07
 **/
//@Aspect
//@Component
public class BoyAspect2 {
    
    @Before("@annotation(test)")
    public void before(JoinPoint joinPoint,Test test) {
        System.out.println("前置通知执行");
    }
    
    @After("@annotation(test)")
    public void after(JoinPoint joinPoint,Test test){
        System.out.println("后置通知执行");
    }
    
    @AfterReturning("@annotation(test)")
    public void returning(JoinPoint joinPoint,Test test){
        System.out.println("返回通知执行");
    }
    
    @AfterThrowing("@annotation(test)")
    public void afterThrowing(JoinPoint joinPoint,Test test){
        System.out.println("异常通知执行");
    }
    
    @Around("@annotation(test)")
    public Object around(ProceedingJoinPoint joinPoint,Test test) throws Throwable{
        System.out.println("环绕通知-原有方法前-执行");
        Object result = joinPoint.proceed();
        System.out.println("环绕通知-原有方法后-执行");
        return result;
    }
}
