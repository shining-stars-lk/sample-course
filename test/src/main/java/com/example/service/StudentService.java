package com.example.service;

import com.example.module.Student;

/**
 * @program: sample-course
 * @description:
 * @author: lk
 * @create: 2024-12-17
 **/
public interface StudentService {
    
    Student get(Long id);
}
