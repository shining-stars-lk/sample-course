package com.example.service.impl;

import com.example.module.Student;
import com.example.service.StudentService;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2024-12-17
 **/
public class StudentServiceXiaoMing
        implements StudentService {
    
    @Override
    public Student get(final Long id) {
        return new Student("小明");
    }

    public void test1(Date start,Date end){
        List<Student> studentList = selectStudents(start, end);
        Map<String,Student> studentMap = new HashMap<>();
        //用studentList和studentMap处理业务逻辑...


    }

    public void test2(Map<String,Student> studentMap) {
        Set<Map.Entry<String, Student>> entries = studentMap.entrySet();
        for (Map.Entry<String, Student> entry : entries) {
            //执行保存业务逻辑... ...
        }
    }

    public List<Student> selectStudents(Date start,Date end){
        return Arrays.asList(new Student("小明"));
    }
}
