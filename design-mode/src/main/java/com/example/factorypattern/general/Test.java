package com.example.factorypattern.general;

/**
 * @program: 工厂模式
 * @description: 葫芦使用
 * @author: 阿星不是程序员
 **/
public class Test {
    
    public static void main(String[] args) {
        
        TianMingMan tianMingMan = new TianMingMan();
        tianMingMan.useGourd("仙品");
    }
}
