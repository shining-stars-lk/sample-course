package com.example.factorypattern.general.impl;

import com.example.factorypattern.general.BaseGourd;

/**
 * @program: 工厂模式
 * @description: 葫芦
 * @author: 阿星不是程序员
 **/
public class WuGunGourd extends BaseGourd {
    @Override
    public void effect() {
        System.out.println("五鬼葫芦：能够回法力三分之二");
    }
}
