package com.example.factorypattern.general;

import com.example.factorypattern.general.impl.WuGunGourd;
import com.example.factorypattern.general.impl.XianPinGourd;

/**
 * @program: 工厂模式
 * @description: 天命人
 * @author: 阿星不是程序员
 **/
public class TianMingMan {

    public void useGourd(String gourdName){
        BaseGourd gourd = null;
        if ("仙品".equals(gourdName)) {
            gourd = new XianPinGourd();
        }else if ("五鬼".equals(gourdName)){
            gourd = new WuGunGourd();
        }
        if (gourd == null) {
            throw new RuntimeException("找不到葫芦");
        }
        gourd.effect();
    }
}
