package com.example.factorypattern.general.impl;

import com.example.factorypattern.general.BaseGourd;

/**
 * @program: 工厂模式
 * @description: 仙品葫芦
 * @author: 阿星不是程序员
 **/
public class XianPinGourd extends BaseGourd {
    
    
    @Override
    public void effect() {
        System.out.println("仙品葫芦：能够回血三分之二");
    }
}
