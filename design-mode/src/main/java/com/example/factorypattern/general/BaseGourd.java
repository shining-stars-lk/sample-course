package com.example.factorypattern.general;

/**
 * @program: 工厂模式
 * @description: 葫芦
 * @author: 阿星不是程序员
 **/
public abstract class BaseGourd {
    
    /**
     * 葫芦生效
     * */
    public abstract void effect();
}
