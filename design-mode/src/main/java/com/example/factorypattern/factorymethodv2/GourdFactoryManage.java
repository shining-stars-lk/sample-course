package com.example.factorypattern.factorymethodv2;


import com.example.factorypattern.factorymethodv2.impl.WuGuiGourdFactory;
import com.example.factorypattern.factorymethodv2.impl.XianPinGourdFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: 工厂模式-工厂方法
 * @description: 仙品葫芦工厂
 * @author: 阿星不是程序员
 **/
public class GourdFactoryManage {
    
    private static final Map<String,AbstractGourdFactory> MAP = new HashMap<>();
    
    static {
        MAP.put("仙品",new XianPinGourdFactory());
        MAP.put("五鬼",new WuGuiGourdFactory());
    }
    
    public static AbstractGourdFactory createFactory(String gourdName) {
        AbstractGourdFactory gourdFactory = MAP.get(gourdName);
        if (gourdFactory == null) {
            throw new RuntimeException("找不到葫芦工厂");
        }
        return gourdFactory;
    }
}
