package com.example.factorypattern.factorymethodv2.impl;


import com.example.factorypattern.factorymethodv2.AbstractGourdFactory;
import com.example.factorypattern.general.BaseGourd;
import com.example.factorypattern.general.impl.XianPinGourd;

/**
 * @program: 工厂模式-工厂方法
 * @description: 仙品葫芦工厂
 * @author: 阿星不是程序员
 **/
public class XianPinGourdFactory extends AbstractGourdFactory {
    
    
    @Override
    public BaseGourd createGourd() {
        return new XianPinGourd();
    }
}
