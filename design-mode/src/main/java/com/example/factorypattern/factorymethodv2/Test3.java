package com.example.factorypattern.factorymethodv2;

import com.example.factorypattern.general.BaseGourd;

/**
 * @program: 工厂模式-工厂方法
 * @description: 葫芦使用
 * @author: 阿星不是程序员
 **/
public class Test3 {
    
    public static void main(String[] args) {
        AbstractGourdFactory gourdFactory = GourdFactoryManage.createFactory("仙品");
        BaseGourd gourd = gourdFactory.createGourd();
        
        TianMingMan tianMingMan = new TianMingMan();
        tianMingMan.setGourd(gourd);
        tianMingMan.useGourd();
        tianMingMan.useGourd();
    }
}
