package com.example.factorypattern.factorymethodv2;

import com.example.factorypattern.general.BaseGourd;

/**
 * @program: 工厂模式-工厂方法
 * @description: 天命人
 * @author: 阿星不是程序员
 **/
public class TianMingMan {
    
    private BaseGourd gourd;

    public void useGourd(){
        if (gourd == null) {
            throw new RuntimeException("葫芦没有设置");
        }
        gourd.effect();
    }
    
    public void setGourd(final BaseGourd gourd) {
        this.gourd = gourd;
    }
}
