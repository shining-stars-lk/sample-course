package com.example.factorypattern.factorymethodv2;

import com.example.factorypattern.general.BaseGourd;

/**
 * @program: 工厂模式-工厂方法
 * @description: 葫芦抽象工厂
 * @author: 阿星不是程序员
 **/
public abstract class AbstractGourdFactory {
    
    /**
     * 生产葫芦
     * @return BaseGourd
     * */
    public abstract BaseGourd createGourd();
}
