package com.example.factorypattern.simplefactory;


import com.example.factorypattern.general.BaseGourd;
import com.example.factorypattern.general.impl.WuGunGourd;
import com.example.factorypattern.general.impl.XianPinGourd;

/**
 * @program: 工厂模式
 * @description: 葫芦工厂
 * @author: 阿星不是程序员
 **/
public class GourdFactory {

    public static BaseGourd create(String gourdName){
        BaseGourd gourd = null;
        if ("仙品".equals(gourdName)) {
            gourd = new XianPinGourd();
        }else if ("五鬼".equals(gourdName)){
            gourd = new WuGunGourd();
        }
        if (gourd == null) {
            throw new RuntimeException("找不到葫芦");
        }
        return gourd;
    }
}
