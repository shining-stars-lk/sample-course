package com.example.factorypattern.simplefactory;

import com.example.factorypattern.general.BaseGourd;

/**
 * @program: 工厂模式
 * @description: 天命人
 * @author: 阿星不是程序员
 **/
public class TianMingMan {

    public void useGourd(String gourdName){
        BaseGourd gourd = GourdFactory.create(gourdName);
        gourd.effect();
    }
}
