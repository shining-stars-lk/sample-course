package com.example.factorypattern.factorymethod;

/**
 * @program: 工厂模式-工厂方法
 * @description: 葫芦使用
 * @author: 阿星不是程序员
 **/
public class Test3 {
    
    public static void main(String[] args) {
        
        TianMingMan tianMingMan = new TianMingMan();
        tianMingMan.useGourd("仙品");
    }
}
