package com.example.factorypattern.factorymethod.impl;

import com.example.factorypattern.factorymethod.AbstractGourdFactory;
import com.example.factorypattern.general.BaseGourd;
import com.example.factorypattern.general.impl.WuGunGourd;

/**
 * @program: 工厂模式-工厂模式-工厂方法
 * @description: 五鬼葫芦工厂
 * @author: 阿星不是程序员
 **/
public class WuGuiGourdFactory extends AbstractGourdFactory {
    
    @Override
    public BaseGourd createGourd() {
        return new WuGunGourd();
    }
}
