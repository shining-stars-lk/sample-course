package com.example.factorypattern.factorymethod;

import com.example.factorypattern.general.BaseGourd;

/**
 * @program: 工厂模式-工厂方法
 * @description: 天命人
 * @author: 阿星不是程序员
 **/
public class TianMingMan {

    public void useGourd(String gourdName){
        AbstractGourdFactory gourdFactory = GourdFactoryManage.createFactory(gourdName);
        BaseGourd gourd = gourdFactory.createGourd();
        gourd.effect();
    }
}
