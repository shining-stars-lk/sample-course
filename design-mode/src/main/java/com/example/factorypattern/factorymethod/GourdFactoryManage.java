package com.example.factorypattern.factorymethod;

import com.example.factorypattern.factorymethod.impl.WuGuiGourdFactory;
import com.example.factorypattern.factorymethod.impl.XianPinGourdFactory;

/**
 * @program: 工厂模式-工厂方法
 * @description: 仙品葫芦工厂
 * @author: 阿星不是程序员
 **/
public class GourdFactoryManage {
    
    public static AbstractGourdFactory createFactory(String gourdName) {
        AbstractGourdFactory gourdFactory = null;
        if ("仙品".equals(gourdName)) {
            gourdFactory = new XianPinGourdFactory();
        }else if ("五鬼".equals(gourdName)) {
            gourdFactory = new WuGuiGourdFactory();
        }
        if (gourdFactory == null) {
            throw new RuntimeException("找不到葫芦工厂");
        }
        return gourdFactory;
    }
}
