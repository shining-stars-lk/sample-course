package com.example.decoratorpattern;

/**
 * @program: 装饰者模式
 * @description: 显示器
 * @author: 阿星不是程序员
 **/
public class DisPlayer implements Computer{
    
    private final Computer computer;
    
    public DisPlayer(Computer computer){
        this.computer = computer;
    }
    
    @Override
    public void use() {
        computer.use();
        System.out.println("实现外接显示器显示画面");
    }
}
