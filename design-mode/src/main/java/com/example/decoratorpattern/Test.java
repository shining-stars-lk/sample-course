package com.example.decoratorpattern;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * @program: 装饰者模式
 * @description: 测试
 * @author: 阿星不是程序员
 **/
public class Test {
    
    public static void main(String[] args) {
        Computer noteBookComputer = new NoteBookComputer();
        Computer disPlayer = new DisPlayer(noteBookComputer);
        Computer sound = new Sound(disPlayer);
        sound.use();
        
        try {
            InputStream fileInputStream = new FileInputStream("");
            InputStream buffInputStream = new BufferedInputStream(fileInputStream);
            InputStream dataInputStream = new DataInputStream(buffInputStream);
            dataInputStream.read();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
