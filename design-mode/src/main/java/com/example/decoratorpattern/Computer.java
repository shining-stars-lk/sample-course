package com.example.decoratorpattern;

/**
 * @program: 装饰者模式
 * @description: 电脑
 * @author: 阿星不是程序员
 **/
public interface Computer {
    
    /**
     * 使用电脑
     * */
    void use();
}
