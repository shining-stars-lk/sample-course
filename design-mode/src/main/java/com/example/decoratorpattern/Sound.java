package com.example.decoratorpattern;

/**
 * @program: 装饰者模式
 * @description: 音响
 * @author: 阿星不是程序员
 **/
public class Sound implements Computer{
    
    private final Computer computer;
    
    public Sound(Computer computer) {
        this.computer = computer;
    }
    
    @Override
    public void use() {
        computer.use();
        System.out.println("使用音响来外放声音");
    }
}
