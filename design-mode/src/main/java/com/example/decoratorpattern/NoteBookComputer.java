package com.example.decoratorpattern;

/**
 * @program: 装饰者模式
 * @description: 笔记本电脑
 * @author: 阿星不是程序员
 **/
public class NoteBookComputer implements Computer{
    
    @Override
    public void use() {
        System.out.println("使用笔记本电脑办公、玩游戏");
    }
}
