package com.example.builderpattern;

import cn.hutool.core.util.StrUtil;

/**
 * @program: 建造者模式
 * @description: 葫芦
 * @author: 阿星不是程序员
 **/
public class Gourd {
    private String name;
    private String colour;
    private Integer grade;
    private Integer slotCount;
    private String extraEffect;
    
    
    private Gourd(Builder builder){
        this.name = builder.name;
        this.colour = builder.colour;
        this.grade = builder.grade;
        this.slotCount = builder.slotCount;
        this.extraEffect = builder.extraEffect;
    }
    
    public static class Builder{
        private static final Integer MIN_GRADE = 5;
        private static final Integer MAX_GRADE = 10;
        private static final Integer MIN_SLOT_COUNT = 1;
        private static final Integer MAX_SLOT_COUNT = 3;
        
        private String name;
        private String colour;
        private Integer grade = MIN_GRADE;
        private Integer slotCount = MIN_SLOT_COUNT;
        private String extraEffect;
        
        public Gourd build(){
            if (StrUtil.isBlank(name)) {
                throw new RuntimeException("名字不能为空");
            }
            if (StrUtil.isBlank(colour)) {
                throw new RuntimeException("颜色不能为空");
            }
            if (grade < MIN_GRADE || grade > MAX_GRADE) {
                throw new RuntimeException("等级不在范围内");
            }
            if (slotCount < MIN_SLOT_COUNT || slotCount > MAX_SLOT_COUNT) {
                throw new RuntimeException("插槽数量不在范围内");
            }
            if (grade <= 7 && slotCount >= 2) {
                throw new RuntimeException("等级小于7的情况下，插槽数量只能有1个");
            }
            return new Gourd(this);
        }
        
        public Builder setName(String name){
            if (StrUtil.isBlank(name)) {
                throw new RuntimeException("名字不能为空");
            }
            this.name = name;
            return this;
        }
        
        public Builder setColour(String colour){
            if (StrUtil.isBlank(colour)) {
                throw new RuntimeException("颜色不能为空");
            }
            this.colour = colour;
            return this;
        }
        
        public Builder setGrade(Integer grade){
            if (grade == null) {
                throw new RuntimeException("等级不能为空");
            }
            if (grade < MIN_GRADE || grade > MAX_GRADE) {
                throw new RuntimeException("等级不在范围内");
            }
            this.grade = grade;
            return this;
        }
        
        public Builder setSlotCount(Integer slotCount){
            if (slotCount == null) {
                throw new RuntimeException("插槽数量不能为空");
            }
            if (slotCount < MIN_SLOT_COUNT || slotCount > MAX_SLOT_COUNT) {
                throw new RuntimeException("插槽数量不在范围内");
            }
            this.slotCount = slotCount;
            return this;
        }
        
        public Builder setExtraEffect(String extraEffect) {
            this.extraEffect = extraEffect;
            return this;
        }
    }
}
