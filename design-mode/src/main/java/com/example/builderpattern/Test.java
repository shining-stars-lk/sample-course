package com.example.builderpattern;

import com.example.builderpattern.Gourd.Builder;

/**
 * @program: 建造者模式
 * @description: 葫芦使用
 * @author: 阿星不是程序员
 **/
public class Test {
    
    public static void main(String[] args) {
        Gourd gourd = new Builder().setName("燃葫芦").setColour("古铜色").setGrade(10).setSlotCount(3).build();
        
    }
}
