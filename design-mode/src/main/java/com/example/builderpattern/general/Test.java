package com.example.builderpattern.general;

/**
 * @program: 建造者模式
 * @description: 葫芦使用
 * @author: 阿星不是程序员
 **/
public class Test {
    
    public static void main(String[] args) {
        
        Gourd gourd1 = new Gourd("燃葫芦","古铜色",5,3,"额外恢复气力");
    
        Gourd gourd2 = new Gourd("燃葫芦","古铜色");
        gourd2.setGrade(5);
        gourd2.setSlotCount(3);
        gourd2.setExtraEffect("额外恢复气力");
    }
}
