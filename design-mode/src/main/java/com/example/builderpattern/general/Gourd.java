package com.example.builderpattern.general;

import cn.hutool.core.util.StrUtil;

/**
 * @program: 建造者模式
 * @description: 葫芦
 * @author: 阿星不是程序员
 **/
public class Gourd {

    private static final Integer MIN_GRADE = 5;
    private static final Integer MAX_GRADE = 10;
    private static final Integer MIN_SLOT_COUNT = 1;
    private static final Integer MAX_SLOT_COUNT = 3;
    
    private String name;
    private String colour;
    private Integer grade = MIN_GRADE;
    private Integer slotCount = MIN_SLOT_COUNT;
    private String extraEffect;
    
    
    public Gourd(String name,String colour,Integer grade,Integer slotCount,String extraEffect){
        if (StrUtil.isBlank(name)) {
            throw new RuntimeException("名字不能为空");
        }
        this.name = name;
        if (StrUtil.isBlank(colour)) {
            throw new RuntimeException("颜色不能为空");
        }
        this.colour = colour;
        if (grade == null) {
            throw new RuntimeException("等级不能为空");
        }
        if (grade < MIN_GRADE || grade > MAX_GRADE) {
            throw new RuntimeException("等级不在范围内");
        }
        this.grade = grade;
        if (slotCount == null) {
            throw new RuntimeException("插槽数量不能为空");
        }
        if (slotCount < MIN_SLOT_COUNT || slotCount > MAX_SLOT_COUNT) {
            throw new RuntimeException("插槽数量不在范围内");
        }
        this.slotCount = slotCount;
        this.extraEffect = extraEffect;
    }
    
    public Gourd(String name,String colour){
        if (StrUtil.isBlank(name)) {
            throw new RuntimeException("名字不能为空");
        }
        this.name = name;
        if (StrUtil.isBlank(colour)) {
            throw new RuntimeException("颜色不能为空");
        }
        this.colour = colour;
    }
    
    public void setGrade(Integer grade){
        if (grade == null) {
            throw new RuntimeException("等级不能为空");
        }
        if (grade < MIN_GRADE || grade > MAX_GRADE) {
            throw new RuntimeException("等级不在范围内");
        }
        this.grade = grade;
    }
    
    public void setSlotCount(Integer slotCount){
        if (slotCount == null) {
            throw new RuntimeException("插槽数量不能为空");
        }
        if (slotCount < MIN_SLOT_COUNT || slotCount > MAX_SLOT_COUNT) {
            throw new RuntimeException("插槽数量不在范围内");
        }
        this.slotCount = slotCount;
    }
    
    public void setExtraEffect(String extraEffect){
        this.extraEffect = extraEffect;
    }
}
