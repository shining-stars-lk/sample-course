package com.example.adapterpattern;

/**
 * @program: 适配器模式
 * @description: 图片管理
 * @author: 阿星不是程序员
 **/
public class ImageManage {

    private final ImageCloud imageCloud;
    
    public ImageManage(ImageCloud imageCloud) {
        this.imageCloud = imageCloud;
    }
    
    public void getInfo(String base64Image) {
        imageCloud.identify(base64Image);
    }
}
