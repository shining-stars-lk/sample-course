package com.example.adapterpattern;

/**
 * @program: 适配器模式
 * @description: 图片云
 * @author: 阿星不是程序员
 **/
public interface ImageCloud {
    
    /**
     * 识别信息
     * @param base64Image 图片base64
     * */
    void identify(String base64Image);
}
