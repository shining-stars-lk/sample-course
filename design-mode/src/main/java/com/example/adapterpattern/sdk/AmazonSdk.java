package com.example.adapterpattern.sdk;

/**
 * @program: 适配器模式
 * @description: 亚马逊云的sdk
 * @author: 阿星不是程序员
 **/
public class AmazonSdk {
    
    public void identifyImage(String base64Image) {
        System.out.println("亚马逊识别出图片");
    }
}
