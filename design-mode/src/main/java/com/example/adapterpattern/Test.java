package com.example.adapterpattern;

import com.example.adapterpattern.impl.AmazonImageCloud;
import com.example.adapterpattern.sdk.AmazonSdk;

/**
 * @program: 适配器模式
 * @description: 测试
 * @author: 阿星不是程序员
 **/
public class Test {
    
    public static void main(String[] args) {
//        ImageCloud imageCloud = new TencentImageCloud();
//        ImageManage imageManage = new ImageManage(imageCloud);
//        imageManage.getInfo("xxxxx");
        
        //模拟创建amazon的sdk
        AmazonSdk amazonSdk = new AmazonSdk();
        ImageCloud imageCloud = new AmazonImageCloud(amazonSdk);
        ImageManage imageManage = new ImageManage(imageCloud);
        imageManage.getInfo("xxxxx");
    }
}
