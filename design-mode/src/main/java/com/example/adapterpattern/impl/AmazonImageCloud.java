package com.example.adapterpattern.impl;

import com.example.adapterpattern.ImageCloud;
import com.example.adapterpattern.sdk.AmazonSdk;

/**
 * @program: 适配器模式
 * @description: 亚马逊云
 * @author: 阿星不是程序员
 **/
public class AmazonImageCloud implements ImageCloud {
    
    private final AmazonSdk amazonSdk;
    
    public AmazonImageCloud(AmazonSdk amazonSdk){
        this.amazonSdk = amazonSdk;
    }
    
    @Override
    public void identify(final String base64Image) {
        amazonSdk.identifyImage(base64Image);
    }
}
