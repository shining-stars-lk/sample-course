package com.example.adapterpattern.impl;

import com.example.adapterpattern.ImageCloud;

/**
 * @program: 适配器模式
 * @description: 腾讯云
 * @author: 阿星不是程序员
 **/
public class TencentImageCloud implements ImageCloud {
    
    @Override
    public void identify(String base64Image) {
        System.out.println("腾讯云识别出身份证图片");    
    }
}
