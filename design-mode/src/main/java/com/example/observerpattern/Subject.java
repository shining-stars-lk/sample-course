package com.example.observerpattern;

/**
 * @program: 观察者模式
 * @description: 被观察者
 * @author: 阿星不是程序员
 **/
public interface Subject {

    void addObserver(Observer observer);
    
    void notifyObserverList(String message);
}
