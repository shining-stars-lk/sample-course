package com.example.observerpattern;

/**
 * @program: 观察者模式
 * @description: 观察着
 * @author: 阿星不是程序员
 **/
public interface Observer {
    
    /**
     * 观察者的动作
     * */
    void add(String message);
}
