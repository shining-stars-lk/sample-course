package com.example.observerpattern.impl;

import com.example.observerpattern.Observer;

/**
 * @program: 观察者模式
 * @description: 快递
 * @author: 阿星不是程序员
 **/
public class ExpressSystemObserver implements Observer {
    @Override
    public void add(final String message) {
        System.out.println("快递系统收到消息："+message+"，开始快递配送");
    }
}
