package com.example.observerpattern.impl;

import com.example.observerpattern.Observer;
import com.example.observerpattern.Subject;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: 观察者模式
 * @description: 用户购买订阅
 * @author: 阿星不是程序员
 **/
public class UserPurchaseSubject implements Subject {
    
    private final List<Observer> observers = new ArrayList<>();
    
    @Override
    public void addObserver(final Observer observer) {
        observers.add(observer);
    }
    
    @Override
    public void notifyObserverList(final String message) {
        for (final Observer observer : observers) {
            observer.add(message);
        }
    }
}
