package com.example.observerpattern.impl;

import com.example.observerpattern.Observer;

/**
 * @program: 观察者模式
 * @description: 积分
 * @author: 阿星不是程序员
 **/
public class PointSystemObserver implements Observer {
    
    @Override
    public void add(final String message) {
        System.out.println("积分系统收到消息："+message+"，积分+1");    
    }
}
