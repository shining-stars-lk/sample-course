package com.example.observerpattern;

import com.example.observerpattern.impl.ExpressSystemObserver;
import com.example.observerpattern.impl.PointSystemObserver;
import com.example.observerpattern.impl.UserPurchaseSubject;

/**
 * @program: 观察者模式
 * @description: 测试
 * @author: 阿星不是程序员
 **/
public class Test {
    
    public static void main(String[] args) {
        UserPurchaseSubject userPurchaseSubject = new UserPurchaseSubject();
        userPurchaseSubject.addObserver(new PointSystemObserver());
        userPurchaseSubject.addObserver(new ExpressSystemObserver());
        userPurchaseSubject.notifyObserverList("用户购买了iPhone");
    }
}
