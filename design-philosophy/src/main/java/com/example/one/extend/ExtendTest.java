package com.example.one.extend;

/**
 * @program: 黑神话悟空-学习程序设计的思想-继承
 * @description: 测试
 * @author: 阿星不是程序员
 **/
public class ExtendTest {
    
    public static void main(String[] args) {
        GreenGourd greenGourd = new GreenGourd();
        greenGourd.addEffect();
        
        BlueGourd blueGourd = new BlueGourd();
        blueGourd.addEffect();
    }
}
