package com.example.one.extend;

import com.example.one.polymorphic.GourdAssist;

/**
 * @program: 黑神话悟空-学习程序设计的思想-继承
 * @description: 绿葫芦(加血)
 * @author: 阿星不是程序员
 **/
public class GreenGourd extends Gourd implements GourdAssist {

    @Override
    public void addEffect(){
        drink();
        System.out.println("成功恢复200血量");
    }
}
