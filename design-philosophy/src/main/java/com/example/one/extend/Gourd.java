package com.example.one.extend;

/**
 * @program: 黑神话悟空-学习程序设计的思想-继承
 * @description: 葫芦
 * @author: 阿星不是程序员
 **/
public class Gourd {

    protected void drink(){
        System.out.println("拿起葫芦来喝");
    }
}
