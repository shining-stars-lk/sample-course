package com.example.one.polymorphic;

import com.example.one.extend.BlueGourd;
import com.example.one.extend.GreenGourd;

/**
 * @program: 黑神话悟空-学习程序设计的思想-多态
 * @description: 测试
 * @author: 阿星不是程序员
 **/
public class PolymorphicTest {
    
    public static void main(String[] args) {
        SunWuKong sunWuKong = new SunWuKong();
        sunWuKong.lightAttack();
        sunWuKong.heavyAttack();
        sunWuKong.useGourd(new BlueGourd());
        sunWuKong.useGourd(new GreenGourd());
    }
}
