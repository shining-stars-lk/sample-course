package com.example.one.polymorphic;

/**
 * @program: 黑神话悟空-学习程序设计的思想-多态
 * @description: 葫芦作用
 * @author: 阿星不是程序员
 **/
public interface GourdAssist {
    
    /**
     * 使用葫芦帮助
     * */
    void addEffect();
}
