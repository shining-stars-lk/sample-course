package com.example.one.polymorphic;

/**
 * @program: 黑神话悟空-学习程序设计的思想-多态
 * @description: 孙悟空
 * @author: 阿星不是程序员
 **/
public class SunWuKong {
    
    public void lightAttack(){
        System.out.println("轻攻击");
    }
    
    public void heavyAttack(){
        System.out.println("重攻击");
    }
    
    public void useGourd(GourdAssist gourdAssist){
        gourdAssist.addEffect();
    }
}
