package com.example.one.abstractexplain;

/**
 * @program: 黑神话悟空-学习程序设计的思想-抽象类
 * @description: 抽象类葫芦
 * @author: 阿星不是程序员
 **/
public abstract class BaseGourd {
    
    private Integer value = 100;
    
    private void drink(){
        System.out.println("拿起葫芦来喝，剩余值:" + --value);
    }
    
    public void addEffect(){
        drink();
        doAddEffect();
    }
    
    /**
     * 具体的葫芦加功效
     * */
    public abstract void doAddEffect();
}
