package com.example.one.abstractexplain;

/**
 * @program: 黑神话悟空-学习程序设计的思想-抽象类
 * @description: 测试
 * @author: 阿星不是程序员
 **/
public class AbstractTest {
    
    public static void main(String[] args) {
        BaseGourd blueGourd = new BlueGourd();
        blueGourd.addEffect();
        
        BaseGourd greenGourd = new GreenGourd();
        greenGourd.addEffect();
    }
}
