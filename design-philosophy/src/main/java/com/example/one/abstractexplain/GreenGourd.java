package com.example.one.abstractexplain;

/**
 * @program: 黑神话悟空-学习程序设计的思想-继承
 * @description: 绿葫芦(加血)
 * @author: 阿星不是程序员
 **/
public class GreenGourd extends BaseGourd {
    @Override
    public void doAddEffect() {
        System.out.println("成功恢复200血量");
    }
}
