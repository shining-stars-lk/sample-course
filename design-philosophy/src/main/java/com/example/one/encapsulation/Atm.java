package com.example.one.encapsulation;

/**
 * @program: 学习程序设计的思想-封装
 * @description: Atm
 * @author: 阿星不是程序员
 **/
public class Atm {
    private static final String SECRET_KEY = "xxxx";
    private Integer balance;
    public Atm(String secretKey,Integer balance){
        if (!SECRET_KEY.equals(secretKey)) {
            throw new RuntimeException("秘钥不正确");
        }
        if (balance < 0) {
            throw new RuntimeException("金额不能小于0");
        }
        this.balance = balance;
    }
    public Integer drawMoney(Integer amount,BankCard bankCard){
        if (bankCard.cardBalance < amount) {
            throw new RuntimeException("银行卡余额不足，不能提供取款服务");
        }
        if (balance < bankCard.cardBalance) {
            throw new RuntimeException("atm余额不足，不能提供取款服务");
        }
        bankCard.cardBalance = bankCard.cardBalance - amount;
        balance = balance - amount;
        System.out.println("取款金额:"+amount);
        System.out.println("银行卡余额:"+bankCard.cardBalance);
        System.out.println("atm余额:"+balance);
        return bankCard.cardBalance;
    }
}
