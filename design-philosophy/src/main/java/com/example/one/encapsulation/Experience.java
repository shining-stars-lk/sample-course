package com.example.one.encapsulation;

/**
 * @program: 黑神话悟空-学习程序设计的思想-封装
 * @description: 经验值
 * @author: 阿星不是程序员
 **/
public class Experience {
    
    /**
     * 技能点
     * */
    private Integer skillValue = 0;
    
    /**
     * 经验值
     * */
    private Integer experienceValue = 0;
    
    /**
     * 等级
     * */
    private Integer gradeValue = 0;
    
    
    public void increaseExperienceValue(){
        // 每次调用增加200经验值
        experienceValue += 200;
        // 计算经验值整千部分
        int newSkillValue = experienceValue / 1000;
        // 如果当前整千部分大于当前技能点数，说明需要增加技能点
        if (newSkillValue > skillValue) {
            skillValue++;
            System.out.println("技能点数增加了！当前技能点数为："+skillValue);
        }
        // 计算等级值整5000部分
        int newGradeValue = experienceValue / 5000;
        // 如果当前整5000部分部分大于当前等级数，说明需要增加等级
        if (newGradeValue > gradeValue) {
            //等级增加
            gradeValue++;
            //技能增加
            skillValue++;
            System.out.println("等级增加了！当前等级为："+gradeValue + "技能点数增加了！当前技能点数为：" + skillValue);
        }
    }
    
    public static void main(String[] args) {
        Experience experience = new Experience();
        for (int i = 0; i < 30; i++) {
            experience.increaseExperienceValue();
        }
    }
}
