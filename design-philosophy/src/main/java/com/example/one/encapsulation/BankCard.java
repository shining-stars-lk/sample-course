package com.example.one.encapsulation;

/**
 * @program: 学习程序设计的思想-封装
 * @description: 银行卡
 * @author: 阿星不是程序员
 **/
public class BankCard {
    
    public String cardNumber;
    
    public Integer cardBalance;
    
    public BankCard(String cardNumber,Integer cardBalance) {
        this.cardNumber = cardNumber;
        this.cardBalance = cardBalance;
    }
}
