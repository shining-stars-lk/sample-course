package com.example.one.encapsulation;

/**
 * @program: 学习程序设计的思想-封装
 * @description: 单例
 * @author: 阿星不是程序员
 **/
public class Singleton {
    private volatile static Singleton instance;
    private Singleton() {}
    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}