package com.example.one.encapsulation;

/**
 * @program: 学习程序设计的思想-封装
 * @description: 测试
 * @author: 阿星不是程序员
 **/
public class Test {
    
    public static void main(String[] args) {
        Atm atm = new Atm("xxxx",100000);
        
        BankCard bankCard = new BankCard("0001",10000);
        
        atm.drawMoney(1000,bankCard);
    }
}
