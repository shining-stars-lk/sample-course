package com.example.three.openclosedprinciple.before;

/**
 * @program: 黑神话悟空-学习程序设计的思想-开闭原则
 * @description: 金箍棒
 * @author: 阿星不是程序员
 **/
public class JinGuBang {

    public void attack(String type){
        if ("劈棍".equals(type)) {
            System.out.println("劈棍式攻击");
        }else if ("立棍".equals(type)) {
            System.out.println("立棍式攻击");
        }
    }
}
