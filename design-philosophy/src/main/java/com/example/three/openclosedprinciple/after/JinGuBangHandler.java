package com.example.three.openclosedprinciple.after;

/**
 * @program: 黑神话悟空-学习程序设计的思想-开闭原则
 * @description: 金箍棒处理
 * @author: 阿星不是程序员
 **/
public abstract class JinGuBangHandler {

    /**
     * 攻击
     * */
    public abstract void attack();
}
