package com.example.three.openclosedprinciple.after.impl;

import com.example.three.openclosedprinciple.after.JinGuBangHandler;

/**
 * @program: 黑神话悟空-学习程序设计的思想-开闭原则
 * @description: 戳棍
 * @author: 阿星不是程序员
 **/
public class ChuoGunHandler extends JinGuBangHandler {
    
    @Override
    public void attack() {
        System.out.println("戳棍式攻击");    
    }
}
