package com.example.three.openclosedprinciple.after;

/**
 * @program: 黑神话悟空-学习程序设计的思想-开闭原则
 * @description: 测试
 * @author: 阿星不是程序员
 **/
public class AfterTest {
    
    public static void main(String[] args) {
        JinGuBang jinGuBang = new JinGuBang();
        jinGuBang.attack("立棍");
    }
}
