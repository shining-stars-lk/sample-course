package com.example.three.openclosedprinciple.after;

import com.example.three.openclosedprinciple.after.impl.ChuoGunHandler;
import com.example.three.openclosedprinciple.after.impl.LiGunHandler;
import com.example.three.openclosedprinciple.after.impl.PiGunHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @program: 黑神话悟空-学习程序设计的思想-开闭原则
 * @description: 金箍棒上下文
 * @author: 阿星不是程序员
 **/
public class JinGuBangContext {

    private static final Map<String,JinGuBangHandler> MAP = new HashMap<>();
    
    static {
        MAP.put("立棍",new LiGunHandler());
        MAP.put("劈棍",new PiGunHandler());
        MAP.put("戳棍",new ChuoGunHandler());
    }
    
    public static JinGuBangHandler get(String type){
        return Optional.ofNullable(MAP.get(type)).orElseThrow(() -> new RuntimeException("找不到对应类型"));
    }
}
