package com.example.three.openclosedprinciple.after;

/**
 * @program: 黑神话悟空-学习程序设计的思想-开闭原则
 * @description: 金箍棒
 * @author: 阿星不是程序员
 **/
public class JinGuBang {

    public void attack(String type){
        JinGuBangContext.get(type).attack();
    }
}
