package com.example.interfacesegregationprinciple;

/**
 * @program: 黑神话悟空-学习程序设计的思想-接口隔离原则
 * @description: 测试
 * @author: 阿星不是程序员
 **/
public class AttackHandler {

    /**
     * 技能值
     * */
    private Integer skillValue;
    
    /**
     * 变换形态指
     */
    private Integer transformFormValue;
    
    public AttackHandler(Integer skillValue, Integer transformFormValue) {
        this.skillValue = skillValue;
        this.transformFormValue = transformFormValue;
    }
    
    public void attack(){
        System.out.println("普通棍势攻击");
        if (skillValue != null && skillValue >0) {
            System.out.println("技能攻击");
        }else {
            System.out.println("技能值不够，无法发动技能攻击");
        }
        if (transformFormValue != null && transformFormValue >0) {
            System.out.println("变换形态攻击");
        }else {
            System.out.println("变换形态值不够，无法发动变换形态攻击");
        }
    }
    
    public void normalAttack(){
        System.out.println("普通棍势攻击");
    }
    
    
    public void skillAttack(){
        if (skillValue != null && skillValue >0) {
            System.out.println("技能攻击");
        }else {
            System.out.println("技能值不够，无法发动技能攻击");
        }
    }
    
    public void transformFormAttack(){
        if (transformFormValue != null && transformFormValue >0) {
            System.out.println("变换形态攻击");
        }else {
            System.out.println("变换形态值不够，无法发动变换形态攻击");
        }
    }
}
