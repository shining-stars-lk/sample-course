package com.example.five.dry.exampl2;

import lombok.Data;

/**
 * @program: 学习程序设计的思想-dry原则
 * @description: 地区实体对象
 * @author: 阿星不是程序员
 **/
@Data
public class Concert {

    private Long id;
    
    private Long name;
    
    private Area area;
}
