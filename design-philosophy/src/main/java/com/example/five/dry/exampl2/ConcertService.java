package com.example.five.dry.exampl2;

/**
 * @program: 学习程序设计的思想-dry原则
 * @description: 演唱会
 * @author: 阿星不是程序员
 **/
public class ConcertService {
    
    private final ConcertMapper concertMapper;
    
    private final AreaService areaService;
    
    public ConcertService(final ConcertMapper concertMapper, final AreaService areaService) {
        this.concertMapper = concertMapper;
        this.areaService = areaService;
    }
    
    public Concert get(Long concertId,Long areaId){
        if (concertId == null) {
            throw new RuntimeException("concertId为空");
        }
        Concert concert = concertMapper.getByConcertId(concertId);
        if (concert == null) {
            throw new RuntimeException("concert为空");
        }
        if (areaId == null) {
            throw new RuntimeException("areaId为空");
        }
        Area area = areaService.getArea(areaId);
        concert.setArea(area);
        return concert;
    }
}
