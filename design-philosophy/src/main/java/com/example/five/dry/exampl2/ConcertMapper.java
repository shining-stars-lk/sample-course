package com.example.five.dry.exampl2;

/**
 * @program: 学习程序设计的思想-dry原则
 * @description: 演唱会Mapper
 * @author: 阿星不是程序员
 **/
public interface ConcertMapper {

    /**
     * 查询演唱会
     * @param concertId 演唱会id
     * @return 演唱会数据
     * */
    Concert getByConcertId(Long concertId);
}
