package com.example.five.dry.exampl2;


/**
 * @program: 学习程序设计的思想-dry原则
 * @description: 地区
 * @author: 阿星不是程序员
 **/
public class AreaService {
    
    private final AreaMapper areaMapper;
    
    public AreaService(final AreaMapper areaMapper) {
        this.areaMapper = areaMapper;
    }
    
    public Area getArea(Long areaId){
        if (areaId == null) {
            throw new RuntimeException("areaId为空");
        }
        //从数据库查询获得地区
        return areaMapper.getByAreaId(areaId);
    }
}
