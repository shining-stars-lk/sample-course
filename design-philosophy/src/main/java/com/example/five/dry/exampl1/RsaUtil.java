package com.example.five.dry.exampl1;

import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;

/**
 * @program: 学习程序设计的思想-dry原则
 * @description: rsa另一个工具类
 * @author: 阿星不是程序员
 **/
public class RsaUtil {
    
    /**
     * 根据公钥串对数据进行RSA加密
     *
     * @param data 数据
     * @param publicKey 公钥
     * @return 加密后
     */
    public static String encryptData(String data, String publicKey){
        RSA rsa = new RSA(null, publicKey);
        byte[] encrypt = rsa.encrypt(data, KeyType.PublicKey);
        return new String(encrypt);
    }
}
