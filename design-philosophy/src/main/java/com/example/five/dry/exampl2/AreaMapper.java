package com.example.five.dry.exampl2;

/**
 * @program: 学习程序设计的思想-dry原则
 * @description: 地区Mapper
 * @author: 阿星不是程序员
 **/
public interface AreaMapper {

    /**
     * 查询地区
     * @param areaId 地区id
     * @return 地区数据
     * */
    Area getByAreaId(Long areaId);
}
