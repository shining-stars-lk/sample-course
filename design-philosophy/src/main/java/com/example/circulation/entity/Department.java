package com.example.circulation.entity;

import lombok.Data;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-02-13
 **/
@Data
public class Department {

    private String name;
}
