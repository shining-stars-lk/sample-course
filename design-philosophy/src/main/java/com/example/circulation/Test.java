package com.example.circulation;

import com.example.circulation.entity.Department;
import com.example.circulation.entity.Employee;
import com.example.circulation.strategy.ComputerSystem;
import com.example.circulation.strategy.factory.ComputerContext;
import com.example.circulation.entity.Course;
import com.example.circulation.vo.StudentVo;
import com.example.module.School;
import com.example.module.Student;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-02-11
 **/
public class Test {
    
    public void checkProgrammer(Programmer programmer){
        if (programmer != null){
            if (programmer.getLevel() != null){
                if (programmer.getLevel().equals("普通员工")){
                    if (programmer.getAge() > 35) {
                        java.lang.System.out.println("被优化了...");
                    }
                }
            }
        }
    }
    
    public Integer getSalary(String level){
        if (level.equals("普通员工")){
            return 10000;
        } else if (level.equals("中级员工")){
            return 20000;
        } else if (level.equals("高级员工")){
            return 30000;
        } else if (level.equals("经理")){
            return 40000;
        }
        return 0;
    }

    public Integer getSalaryV2(String level){
        switch (level){
            case "普通员工":
                return 10000;
            case "中级员工":
                return 20000;
            case "高级员工":
                return 30000;
            case "经理":
                return 40000;
            default:
                return 0;
        }
    }
    Map<String, Integer> map = new HashMap<>(){{
        put("普通员工", 10000);
        put("中级员工", 20000);
        put("高级员工", 30000);
        put("经理", 40000);
    }};
    
    public Integer getSalaryV3(String level){
        return Optional.ofNullable(map.get(level))
                .orElse(0);
    }
    
    public StudentVo getStudentVo(
            Long studentId, Long courseId,Long schoolId){
        Student student = getStudentById(studentId);
        Course course = getCourseById(courseId);
        School school = getSchoolById(schoolId);
        if (student != null){
            if (course != null){
                if (school != null){
                    StudentVo studentVo = new StudentVo();
                    studentVo.setName(student.getName());
                    studentVo.setCourseName(course.getName());
                    studentVo.setSchoolName(school.getName());
                    return studentVo;
                }else {
                    throw new RuntimeException("学校不存在");
                }
            }else {
                throw new RuntimeException("课程不存在");
            }
        }else {
            throw new RuntimeException("学生不存在");
        }
    }
    
    public StudentVo getStudentVoV2(
            Long studentId, Long courseId,Long schoolId){
        Student student = getStudentById(studentId);
        Course course = getCourseById(courseId);
        School school = getSchoolById(schoolId);
        if (student == null){
            throw new RuntimeException("学生不存在");
        }
        if (course == null){
            throw new RuntimeException("课程不存在");
        }
        if (school == null){
            throw new RuntimeException("学校不存在");
        }
        StudentVo studentVo = new StudentVo();
        studentVo.setName(student.getName());
        studentVo.setCourseName(course.getName());
        studentVo.setSchoolName(school.getName());
        return studentVo;
    }
    
    public StudentVo getStudentVoV3(
            Long studentId, Long courseId,Long schoolId){
        Student student = getStudentById(studentId);
        Course course = getCourseById(courseId);
        School school = getSchoolById(schoolId);
        check(student, course, school);
        StudentVo studentVo = new StudentVo();
        studentVo.setName(student.getName());
        studentVo.setCourseName(course.getName());
        studentVo.setSchoolName(school.getName());
        return studentVo;
    }
    
    public void check(
            Student student, Course course,
            School school){
        if (student == null) {
            throw new RuntimeException("学生不存在");
        }
        if (course == null) {
            throw new RuntimeException("课程不存在");
        }
        if (school == null) {
            throw new RuntimeException("学校不存在");
        }
    }
    
    public Student getStudentById(Long id){
        return new Student();
    }
    
    public Course getCourseById(Long id){
        return new Course();
    }
    
    public School getSchoolById(Long id){
        return new School();
    }
    
    
    public void computerSystemWork(String systemType){
        if ("windows".equals(systemType)) {
            System.out.println("windows系统工作");
        }else if ("linux".equals(systemType)) {
            System.out.println("linux系统工作");
        }else if ("mac".equals(systemType)) {
            System.out.println("mac系统工作");
        }
    }
    
    public void computerWorkV2(String computerType){
        ComputerContext.create(computerType).work();
    }
    
    public void computerWorkV3(String computerType){
        Optional.ofNullable(ComputerContext.create(computerType))
                .ifPresent(ComputerSystem::work);
    }
    
    public List<Programmer> fiterProgrammer(
            List<Programmer> programmerList){
        List<Programmer> seniorProgrammerList = new ArrayList<>();
        for (Programmer programmer : programmerList) {
            if (programmer.getAge() > 25) {
                if (programmer.getLevelCode() > 1) {
                    seniorProgrammerList.add(programmer);
                }
            }
        }
        return seniorProgrammerList;
    }
    
    public List<Programmer> fiterProgrammerV2(
            List<Programmer> programmerList){
        return programmerList.stream()
                .filter(programmer -> programmer.getAge() > 25)
                .filter(programmer -> programmer.getLevelCode() > 1)
                .collect(Collectors.toList());
    }
    
    public String selectDepartmentName(Employee employee) {
        if (employee != null) {
            if (employee.getDepartment() != null) {
                if (employee.getDepartment().getName() != null) {
                    return employee.getDepartment().getName();
                }
            }
        }
        return "";
    }
    
    public String selectDepartmentNameV2(Employee employee) {
        return Optional.ofNullable(employee)
                .map(Employee::getDepartment)
                .map(Department::getName)
                .orElse("");
    }
    
    
    public static void main(String[] args) {
        Test test = new Test();
        test.checkProgrammer(new Programmer());
        test.getSalary("普通员工");
        test.getSalaryV2("普通员工");
        test.getSalaryV3("普通员工");
        test.getStudentVo(1L, 2L, 3L);
        test.getStudentVoV2(1L, 2L, 3L);
        test.getStudentVoV3(1L, 2L, 3L);
        test.computerSystemWork("windows");
        test.computerWorkV2("windows");
        test.computerWorkV3("windows");
        test.fiterProgrammer(new ArrayList<>());
        test.fiterProgrammerV2(new ArrayList<>());
        test.selectDepartmentName(new Employee());
        test.selectDepartmentNameV2(new Employee());
    }
}
