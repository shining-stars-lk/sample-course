package com.example.circulation.strategy;

/**2
 * @program: sample-course
 * @description:
 * @author: lk
 * @create: 2025-02-13
 **/
public interface ComputerSystem {
    
    void work();
}
