package com.example.circulation.strategy.factory;

import com.example.circulation.strategy.ComputerSystem;
import com.example.circulation.strategy.impl.Linux;
import com.example.circulation.strategy.impl.Mac;
import com.example.circulation.strategy.impl.Windows;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-02-13
 **/
public class ComputerContext {
    
    static Map<String, ComputerSystem> map =
            new HashMap<>(){{
        put("windows", new Windows());
        put("mac", new Mac());
        put("linux", new Linux());
    }};
    
    public static ComputerSystem create(
            String type) {
        return map.get(type);
    }
}
