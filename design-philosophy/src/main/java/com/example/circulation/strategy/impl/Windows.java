package com.example.circulation.strategy.impl;

import com.example.circulation.strategy.ComputerSystem;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-02-13
 **/
public class Windows implements ComputerSystem {
    @Override
    public void work() {
        System.out.println("windows系统工作");
    }
}
