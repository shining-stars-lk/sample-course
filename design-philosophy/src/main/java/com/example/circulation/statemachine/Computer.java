package com.example.circulation.statemachine;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-02-13
 **/
public class Computer {
    
    private Integer state = State.OFF;
    
    public void powerOn() {
        if (state.equals(State.OFF)) {
            this.state = State.ON;
            System.out.println("开机");
        } else if (state.equals(State.ON)) {
            System.err.println("已经开机，不能再开机");
        } else if (state.equals(State.WORK)) {
            System.err.println("在工作中，不能再开机");
        }
    }
    
    public void powerOff() {
        if (state.equals(State.OFF)) {
            System.err.println("已经关机，不能再关机");
        } else if (state.equals(State.ON)) {
            System.err.println("还没有工作，不能关机");
        } else if (state.equals(State.WORK)) {
            this.state = State.OFF;
            System.out.println("关机了");
        }
    }
    
    public void work() {
        if (state.equals(State.OFF)) {
            System.err.println("已经关机，不能工作");
        } else if (state.equals(State.ON)) {
            this.state = State.WORK;
            System.out.println("工作中");
        } else if (state.equals(State.WORK)) {
            System.err.println("已经是在工作中了");
        }
    }
}
