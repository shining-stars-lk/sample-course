package com.example.circulation.statemachine.optimize.impl;

import com.example.circulation.statemachine.optimize.Computer;
import com.example.circulation.statemachine.optimize.ComputerState;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-02-13
 **/
public class OffState extends ComputerState {
    
    public OffState(Computer computer) {
        super(computer);
    }
    
    @Override
    public void powerOn() {
        System.out.println("开机");
        super.computer.setState(computer.getOnState());
    }
    
    @Override
    public void powerOff() {
        System.err.println("已经关机了，不能再关机");
    }
    
    @Override
    public void work() {
        System.err.println("已经关机了，不能工作");
    }
}
