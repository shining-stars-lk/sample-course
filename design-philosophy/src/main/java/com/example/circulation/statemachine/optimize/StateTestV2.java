package com.example.circulation.statemachine.optimize;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-02-13
 **/
public class StateTestV2 {
    
    public void state(){
        Computer computer = new Computer();
        computer.powerOn();
        computer.work();
        computer.powerOff();
    }
    
    public static void main(String[] args) {
        StateTestV2 stateTest = new StateTestV2();
        stateTest.state();
    }
}
