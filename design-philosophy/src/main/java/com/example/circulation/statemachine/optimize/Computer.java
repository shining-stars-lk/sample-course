package com.example.circulation.statemachine.optimize;

import com.example.circulation.statemachine.optimize.impl.OffState;
import com.example.circulation.statemachine.optimize.impl.OnState;
import com.example.circulation.statemachine.optimize.impl.WorkState;
import lombok.Data;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-02-13
 **/
@Data
public class Computer {
    
    private ComputerState offState = new OffState(this);
    
    private ComputerState onState = new OnState(this);
    
    private ComputerState workState = new WorkState(this);
    
    private ComputerState state = offState;
    
    public void powerOn() {
        this.state.powerOn();
    }
    
    public void powerOff() {
        this.state.powerOff();
    }
    
    public void work() {
        this.state.work();
    }
}
