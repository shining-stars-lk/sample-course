package com.example.circulation.statemachine;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-02-13
 **/
public class StateTestV1 {
    
    public void state(){
        Computer computer = new Computer();
        computer.powerOn();
        computer.work();
        computer.powerOff();
    }
    
    public static void main(String[] args) {
        StateTestV1 stateTestV1 = new StateTestV1();
        stateTestV1.state();
    }
}
