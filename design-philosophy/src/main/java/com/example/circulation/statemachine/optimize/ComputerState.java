package com.example.circulation.statemachine.optimize;

/**
 * @program: sample-course
 * @description:
 * @author: lk
 * @create: 2025-02-13
 **/
public abstract class ComputerState {
    
    protected Computer computer;
    
    public ComputerState(Computer computer) {
        this.computer = computer;
    }
    
    public abstract void powerOn();
        
    public abstract void powerOff();
        
    public abstract void work();
}
