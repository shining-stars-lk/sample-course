package com.example.circulation.statemachine.optimize.impl;

import com.example.circulation.statemachine.optimize.Computer;
import com.example.circulation.statemachine.optimize.ComputerState;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-02-13
 **/
public class OnState extends ComputerState {
    
    public OnState(Computer computer) {
        super(computer);
    }
    
    @Override
    public void powerOn() {
        System.err.println("已经开机了，不能再开机");
    }
    
    @Override
    public void powerOff() {
        System.err.println("还没有工作，不能关机");
        super.computer.setState(computer.getOffState());
    }
    
    @Override
    public void work() {
        System.out.println("在工作中");
        super.computer.setState(computer.getWorkState());
    }
}
