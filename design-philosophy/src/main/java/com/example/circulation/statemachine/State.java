package com.example.circulation.statemachine;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-02-13
 **/
public class State {
    
    public static final Integer OFF = 1;
    
    public static final Integer ON = 2;
    
    public static final Integer WORK = 3;
}
