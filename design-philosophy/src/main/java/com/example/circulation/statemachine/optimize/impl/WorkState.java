package com.example.circulation.statemachine.optimize.impl;

import com.example.circulation.statemachine.optimize.Computer;
import com.example.circulation.statemachine.optimize.ComputerState;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-02-13
 **/
public class WorkState extends ComputerState {
    
    public WorkState(Computer computer) {
        super(computer);
    }
    
    @Override
    public void powerOn() {
        System.err.println("已经开机了，不能再开机");
    }
    
    @Override
    public void powerOff() {
        System.out.println("关机");
        super.computer.setState(computer.getOffState());
    }
    
    @Override
    public void work() {
        System.err.println("已经在工作中了，无需再切换");
    }
}
