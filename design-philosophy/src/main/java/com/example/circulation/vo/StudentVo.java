package com.example.circulation.vo;

import lombok.Data;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-02-13
 **/
@Data
public class StudentVo {
    
    private String name;
    
    private String courseName;
    
    private String schoolName;
}
