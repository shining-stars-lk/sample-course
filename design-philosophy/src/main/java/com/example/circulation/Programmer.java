package com.example.circulation;

import lombok.Data;

@Data
public class Programmer {
    
    private String name;

    private Integer age;
    
    private String level;
    
    private Integer levelCode;
    
}
