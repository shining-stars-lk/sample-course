package com.example.two.part2;

/**
 * @program: 黑神话悟空-学习程序设计的思想-抽象类
 * @description: 灵虚子boss
 * @author: 阿星不是程序员
 **/
public class LingXuZiBoss extends BaseWeaknessBoss {
    
    @Override
    public void attack() {
        System.out.println("咬人攻击");
    }
    
    @Override
    public void weakness() {
        System.out.println("弱点是毛多弱火");
    }
}
