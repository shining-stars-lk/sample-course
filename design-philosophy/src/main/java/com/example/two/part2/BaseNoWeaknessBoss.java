package com.example.two.part2;

/**
 * @program: 黑神话悟空-学习程序设计的思想-抽象类
 * @description: 没有弱点boss抽象类
 * @author: 阿星不是程序员
 **/
public abstract class BaseNoWeaknessBoss extends BaseBoss {
    
    /**
     * 没有弱点的方法
     * */
    public abstract void noWeakness();
}
