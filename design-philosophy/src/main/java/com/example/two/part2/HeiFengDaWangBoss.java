package com.example.two.part2;

/**
 * @program: 黑神话悟空-学习程序设计的思想-抽象类
 * @description: 黑风大王boss
 * @author: 阿星不是程序员
 **/
public class HeiFengDaWangBoss extends BaseNoWeaknessBoss {
    
    
    @Override
    public void attack() {
        System.out.println("大范围攻击");
    }
    
    @Override
    public void noWeakness() {
        System.out.println("攻击范围很广");
    }
}
