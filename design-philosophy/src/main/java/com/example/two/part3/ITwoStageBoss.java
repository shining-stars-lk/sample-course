package com.example.two.part3;

/**
 * @program: 黑神话悟空-学习程序设计的思想-抽象类
 * @description: boss二阶段接口
 * @author: 阿星不是程序员
 **/
public interface ITwoStageBoss extends IAttackBoss{

    /**
     * 变身二阶段
     * */
    void twoStage();
}
