package com.example.two.part3;

/**
 * @program: 黑神话悟空-学习程序设计的思想-抽象类
 * @description: boss攻击接口
 * @author: 阿星不是程序员
 **/
public interface IAttackBoss {
    
    /**
     * 攻击方法
     * */
     void attack();
}
