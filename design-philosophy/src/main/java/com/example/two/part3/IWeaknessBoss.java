package com.example.two.part3;

/**
 * @program: 黑神话悟空-学习程序设计的思想-抽象类
 * @description: boss弱点接口
 * @author: 阿星不是程序员
 **/
public interface IWeaknessBoss extends IAttackBoss{
    
    /**
     * 弱点方法
     * */
     void weakness();
}
