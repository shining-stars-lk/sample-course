package com.example.two.part3.impl;

import com.example.two.part3.ITwoStageBoss;

/**
 * @program: 黑神话悟空-学习程序设计的思想-抽象类
 * @description: 白衣秀士boss
 * @author: 阿星不是程序员
 **/
public class BaiYiXiuShiBoss implements ITwoStageBoss {
    @Override
    public void attack() {
        System.out.println("攻击速度很快");
    }
    
    @Override
    public void twoStage() {
        System.out.println("变身半蛇身的二阶段");
    }
}
