package com.example.two.part3;

import com.example.two.part3.impl.BaiYiXiuShiBoss;
import com.example.two.part3.impl.HeiFengDaWangBoss;
import com.example.two.part3.impl.LingXuZiBoss;

/**
 * @program: 黑神话悟空-学习程序设计的思想-抽象
 * @description: 测试
 * @author: 阿星不是程序员
 **/
public class Test3 {
    
    public static void main(String[] args) {
        //灵虚子boss
        IWeaknessBoss lingXuZiBoss = new LingXuZiBoss();
        lingXuZiBoss.attack();
        lingXuZiBoss.weakness();
        
        //黑风大王boss
        IAttackBoss heiFengDaWangBoss = new HeiFengDaWangBoss();
        heiFengDaWangBoss.attack();
        
        //白衣秀士boss
        ITwoStageBoss baiYiXiuShiBoss = new BaiYiXiuShiBoss();
        baiYiXiuShiBoss.attack();
        baiYiXiuShiBoss.twoStage();
    }
}
