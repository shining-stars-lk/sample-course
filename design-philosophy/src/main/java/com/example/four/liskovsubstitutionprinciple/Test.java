package com.example.four.liskovsubstitutionprinciple;

/**
 * @program: 黑神话悟空-学习程序设计的思想-里氏替换原则
 * @description: 测试
 * @author: 阿星不是程序员
 **/
public class Test {
    
    public static void main(String[] args) {
        JinGuBang jinGuBang = new JinGuBang();
        jinGuBang.attack();
        JinGuBangEnhance jinGuBangEnhance = new JinGuBangEnhance(1);
        jinGuBangEnhance.attack();
    }
}
