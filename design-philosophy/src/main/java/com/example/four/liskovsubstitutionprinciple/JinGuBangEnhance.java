package com.example.four.liskovsubstitutionprinciple;

/**
 * @program: 黑神话悟空-学习程序设计的思想-开闭原则
 * @description: 金箍棒加强
 * @author: 阿星不是程序员
 **/
public class JinGuBangEnhance extends JinGuBang {
    
    private Integer staminaValue;
    
    public JinGuBangEnhance(Integer staminaValue){
        this.staminaValue = staminaValue;
    }

    @Override
    public void attack(){
        if (staminaValue == null || staminaValue <= 0 ) {
            throw new RuntimeException("体力值不够无法组合攻击");
        }
        super.attack();
        System.out.println("棍势攻击");
        staminaValue--;
    }
}
