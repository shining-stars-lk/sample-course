package com.example.one;

import java.util.concurrent.CountDownLatch;

/**
 * @program: 多线程
 * @description: 测试
 * @author: 阿星不是程序员
 **/
public class Test1 {
    
    public static void main(String[] args) {
        Test1 test1 = new Test1();
        test1.goOut();
    }
    
    public void goOut(){
        long start = System.currentTimeMillis();
        
        CountDownLatch countDownLatch = new CountDownLatch(2);
        
        Girl girl = new Girl();
        new Thread(() -> {
            girl.dressUp();
            countDownLatch.countDown();
        }).start();
        Boy boy = new Boy();
        new Thread(() -> {
            boy.doThing();
            countDownLatch.countDown();
        }).start();
        
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("耗时："+(System.currentTimeMillis() - start));
        System.out.println("出门");
    }
}
