package com.example.one;

/**
 * @program: 多线程
 * @description: 男
 * @author: 阿星不是程序员
 **/
public class Boy {
    
    public void doThing(){
        System.out.println("开始玩游戏");
        System.out.println("游戏中");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("结束玩游戏");
    }
}
