package com.example.one;

/**
 * @program: 多线程
 * @description: 女
 * @author: 阿星不是程序员
 **/
public class Girl {
    
    public void dressUp(){
        System.out.println("开始化妆");
        System.out.println("化妆中");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("结束化妆");
    }
}
