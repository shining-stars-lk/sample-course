package com.example.two;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @program: 多线程
 * @description: 测试
 * @author: 阿星不是程序员
 **/
public class Test2 {
    
    private final Object door = new Object();
    
    public void girlGrabPhone(Phone phone){
        synchronized (door) {
            System.out.println("女孩抢到手机");
            phone.show();
            System.out.println("女孩看完手机聊天记录");
        }
    }
    public void boyGrabPhone(Phone phone) {
        synchronized (door) {
            System.out.println("男孩抢到手机");
            phone.del();
            System.out.println("男孩删完手机聊天记录");
        }
    }
    public static void main(String[] args) {
        
        Test2 test2 = new Test2();
        Phone phone = new Phone();
        
        new Thread(() -> test2.girlGrabPhone(phone)).start();
        
        new Thread(() -> test2.boyGrabPhone(phone)).start();
        
        Lock lock = new ReentrantLock();
        lock.lock();
        lock.unlock();
    }
}
