package com.example.ttl;

import com.alibaba.ttl.TransmittableThreadLocal;
import com.alibaba.ttl.TtlRunnable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-03-05
 **/
public class Test1 {
    
    TransmittableThreadLocal<String> ttl = new TransmittableThreadLocal<>();
    
    ExecutorService executors = Executors.newFixedThreadPool(10);
    
    public void test1(){
        ttl.set("test1");
        executors.submit(() -> {
            System.out.println(ttl.get());
        });
    }
    
    public void test2(){
        ttl.set("test2");
        executors.submit(TtlRunnable.get(() -> {
            System.out.println(ttl.get());
        }));
    }
    
    public static void main(String[] args) {
        Test2 test2 = new Test2();
        test2.test2();
    }
    
}
