package com.example.ttl;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-03-05
 **/
public class Test01 {
    
    public static void main(String[] args) {
        System.out.println("添加数据");
        BaseParameterHolder.setParameter("name", "小红");
        
        ExecutorService executors = Executors.newFixedThreadPool(10);
        
        Map<String, String> parameterMap = BaseParameterHolder.getParameterMap();
        Map<String,String> newMap = new HashMap<>(parameterMap.size());
        newMap.putAll(parameterMap);
        executors.execute(() -> {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("线程池执行");
            
            BaseParameterHolder.setParameterMap(newMap);
            System.out.println(BaseParameterHolder.getParameter("name"));
        });
        System.out.println("删除数据");
        BaseParameterHolder.removeParameter("name");
    }
}
