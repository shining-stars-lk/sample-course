package com.example.ttl;

import com.alibaba.ttl.TransmittableThreadLocal;
import com.alibaba.ttl.TtlRunnable;
import io.micrometer.common.util.StringUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test2 {
    
    TransmittableThreadLocal<String> ttl = new TransmittableThreadLocal<>() {
        @Override
        protected void beforeExecute() {
            String value = get();
            if (StringUtils.isNotEmpty(value)) {
                threadLocal.set(value);
            }
        }
        @Override   
        protected void afterExecute() {
            threadLocal.remove();
        }
    };
    
    ExecutorService executors = Executors.newFixedThreadPool(10);
    
    ThreadLocal<String> threadLocal = new ThreadLocal<>();
    
    public void test1(){
        threadLocal.set("test1");
        executors.submit(() -> {
            System.out.println(threadLocal.get());
        });
    }
    
    public void test2(){
        ttl.set("test2");
        executors.submit(TtlRunnable.get(() -> {
            System.out.println(threadLocal.get());
        }));
    }
    
    public static void main(String[] args) {
        Test2 test2 = new Test2();
        test2.test2();
    }
}
