package com.example.module;

import lombok.Data;

@Data
public class Student {
    
    public String name;

    public String className;
    
    public Student(String name) {
        this.name = name;
    }
    
    public Student(){
        System.out.println("....student对象加载....");
    }
}
