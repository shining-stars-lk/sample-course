package com.example.module;

import lombok.Data;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2025-01-03
 **/
@Data
public class School {

    private Long id;
    
    private String name;
    
    public School(){
        System.out.println("....school对象加载....");
    }
}
