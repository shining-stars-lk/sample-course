package com.example.two;

/**
 * @program: 多线程
 * @description: 手机
 * @author: 阿星不是程序员
 **/
public class Phone {

    public void show(){
        System.out.println("显示聊天内容");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void del(){
        System.out.println("删除聊天内容");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
