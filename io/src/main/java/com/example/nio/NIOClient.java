package com.example.nio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * @program: sample-course
 * @description:
 * @author: k
 * @create: 2024-11-01
 **/
public class NIOClient {
    public static void main(String[] args) throws IOException {
        // 创建一个SocketChannel，并连接到服务端
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.connect(new InetSocketAddress("localhost", 8080));
        // 发送数据到服务端
        String message = "Hello, NIO server!";
        ByteBuffer buffer = ByteBuffer.wrap(message.getBytes());
        socketChannel.write(buffer);
        socketChannel.close();
    }
}
